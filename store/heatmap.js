import config from '../services/config';
import event from '../services/events';

const state = () => ({
	id: config.heatmap.id,
	active: config.heatmap.active,
	class: config.heatmap.class,
	coverage: config.heatmap.props.coverage,
	index: config.heatmap.index,
	params: config.heatmap.params,
	props: config.heatmap.props,
	radius: config.heatmap.props.radius,
	settings: config.heatmap.settings,
	upperPercentile: config.heatmap.props.upperPercentile,
});

const mutations = {
	setActive(state) {
		state.active = !state.active;
	},
	setParams(state, [param, value]) {
		state[param] = value;
	},
	setSettings(state, [key, settings]) {
		state.settings[key] = settings[key];
	},
};

const actions = {
	resetMap({ commit, state }) {
		const settings = state.settings.default;
		Object.keys(settings).map((key) => {
			commit('setSettings', [key, settings]);
			return true;
		});
		event.emit('resetHeatmapSettings');
	},
	resetParams({ state }) {
		state.params.map((param) => {
			this.dispatch('heatmap/setParams', [param, state.props[param]]);
			return true;
		});
		event.emit('resetHeatmapParams');
	},
	setActive({ commit }) {
		commit('setActive');
	},
	setParams({ commit }, [param, value]) {
		commit('setParams', [param, Number(value)]);
	},
	setSettings({ commit }, settings) {
		Object.keys(settings).map((key) => {
			commit('setSettings', [key, settings]);
			return true;
		});
	},
};

const getters = {
	heatmap: state => state,
};

const heatmap = {
	actions,
	getters,
	mutations,
	state,
};

export default heatmap;
