import config from '../services/config';

const state = () => ({
	company: {
		name: config.header.company.name,
		logo: {
			src: config.header.company.logo.src,
		},
	},
	repo: {
		name: config.header.repo.name,
		src: config.header.repo.src,
	},
	title: config.header.title,
});

const getters = {
	header: state => state,
};

const header = {
	getters,
	state,
};

export default header;
