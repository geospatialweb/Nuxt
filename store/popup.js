import config from '../services/config';

const state = () => ({
	map: config.popup.active,
	marker: config.popup.active,
});

const mutations = {
	setMapActive(state) {
		state.map = !state.map;
	},
	setMarkerActive(state) {
		state.marker = !state.marker;
	},
};

const actions = {
	setMapPopupActive({ commit }) {
		commit('setMapActive');
	},
	setMarkerPopupActive({ commit }) {
		commit('setMarkerActive');
	},
};

const popup = {
	actions,
	mutations,
	state,
};

export default popup;
