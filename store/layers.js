import config from '../services/config';
import event from '../services/events';

const state = () => ({
	id: config.layers.id,
	active: config.layers.active,
	layers: config.layers.layers,
});

const mutations = {
	setActive(state, i) {
		state.layers[i].active = !state.layers[i].active;
	},
	setLayersActive(state) {
		state.active = !state.active;
	},
};

const actions = {
	selectLayer(context, evt) {
		event.emit('selectLayer', evt);
	},
	setActive({ commit }, i) {
		commit('setActive', i);
	},
	setLayersActive({ commit }) {
		commit('setLayersActive');
	},
};

const getters = {
	el: state => state,
	layers: state => state.layers,
};

const layers = {
	actions,
	getters,
	mutations,
	state,
};

export default layers;
