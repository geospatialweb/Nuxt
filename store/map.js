import config from '../services/config';

const state = () => ({
	id: config.map.id,
	accessToken: config.map.accessToken,
	class: config.map.class,
	controls: config.map.controls,
	settings: config.map.settings,
	style: config.map.styles.outdoors,
	styles: config.map.styles,
});

const mutations = {
	setActive(state, style) {
		state.styles[style].active = !state.styles[style].active;
	},
	setActiveStyle(state, style) {
		state.style = style;
	},
	setSettings(state, [key, settings]) {
		state.settings[key] = settings[key];
	},
};

const actions = {
	setActiveStyle({ commit, state }, id) {
		let style;
		commit('setActive', id);

		id === state.styles.outdoors.id ?
			style = state.styles.satellite.id :
			style = state.styles.outdoors.id;

		commit('setActive', style);
		commit('setActiveStyle', state.styles[style]);
	},
	setSettings({ commit }, settings) {
		Object.keys(settings).map((key) => {
			commit('setSettings', [key, settings]);
			return true;
		});
	},
};

const getters = {
	map: state => state,
};

const map = {
	actions,
	getters,
	mutations,
	state,
};

export default map;
