import config from '../services/config';

const state = () => ({
	id: config.markers.id,
	markers: [],
	markersHash: {},
});

const mutations = {
	setActive(state, id) {
		state.markers[state.markersHash[id]].active =
			!state.markers[state.markersHash[id]].active;
	},
	setMarkers(state, markers) {
		state.markers.push(markers);
	},
	setMarkersHash(state, [id, i]) {
		state.markersHash[id] = i;
	},
};

const actions = {
	setActive({ commit }, id) {
		commit('setActive', id);
	},
	setMarkers({ commit, state }, markers) {
		commit('setMarkers', markers);

		if (state.markers.length === config.markers.length) {
			this.dispatch('markers/setMarkersHash');
		}
	},
	setMarkersHash({ commit, state }) {
		state.markers.map((marker, i) => {
			const id = marker[0].getElement().classList[0].replace('-marker', '');
			return commit('setMarkersHash', [id, i]);
		});
		this.dispatch('data/loaded', state.id);
	},
};

const markers = {
	actions,
	mutations,
	state,
};

export default markers;
