import config from '../services/config';
import event from '../services/events';

const state = () => ({
	id: config.trails.id,
	active: config.trails.active,
	trails: [],
});

const mutations = {
	setActive(state, [active, i]) {
		state.trails[i].active = active;
	},
	setTrails(state, trail) {
		state.trails.push(trail);
	},
	setTrailsActive(state) {
		state.active = !state.active;
	},
};

const actions = {
	selectTrail(context, evt) {
		event.emit('selectTrail', evt);
	},
	setActive({ commit, state }, i) {
		state.trails.map((trail, idx) => {
			i === idx ?
				commit('setActive', [!config.trails.active, i]) :
				commit('setActive', [config.trails.active, i]);
			return true;
		});
	},
	setTrails({ commit, state }, trails) {
		trails.map((trail) => {
			commit('setTrails', trail);
			return true;
		});
		this.dispatch('data/loaded', state.id);
	},
	setTrailsActive({ commit }) {
		commit('setTrailsActive');
	},
};

const getters = {
	el: state => state,
	trails: state => state.trails,
};

const trails = {
	actions,
	getters,
	mutations,
	state,
};

export default trails;
