import config from '../services/config';

const state = () => ({
	id: config.layerStyles.id,
	biosphere: config.layerStyles.biosphere,
	layerStyles: [],
});

const mutations = {
	setActive(state, i) {
		state.layerStyles[i].active = !state.layerStyles[i].active;
	},
	setLayerStyle(state, layerStyle) {
		state.layerStyles.push(layerStyle);
	},
	setVisibility(state, [i, visible]) {
		state.layerStyles[i].layout.visibility = visible;
	},
};

const actions = {
	setActive({ commit, state }, layerStyle) {
		const i = state.layerStyles.findIndex(obj => obj.id === layerStyle);
		commit('setActive', i);
		state.layerStyles[i].active ?
			commit('setVisibility', [i, 'visible']) :
			commit('setVisibility', [i, 'none']);
	},
	setLayerStyle({ commit }, layerStyle) {
		commit('setLayerStyle', layerStyle);
	},
};

const layerStyles = {
	actions,
	mutations,
	state,
};

export default layerStyles;
