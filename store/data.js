import config from '../services/config';
import event from '../services/events';

const state = () => ({
	loaded: config.data.loaded,
	heatmap: config.data.loaded,
	layerStyles: config.data.loaded,
	markers: config.data.loaded,
	trails: config.data.loaded,
});

const mutations = {
	loaded(state, id) {
		state[id] = !state[id];
	},
	dataLoaded(state) {
		state.loaded = !state.loaded;
	},
};

const actions = {
	loaded({ commit }, id) {
		commit('loaded', id);
		this.dispatch('data/dataLoaded');
	},
	dataLoaded({ commit, state }) {
		const notLoaded = Object.keys(state)
			.filter((id, i) => {
				if (i > 0 && !state[id]) {
					return true;
				}
				return false;
			});

		if (!notLoaded.length) {
			commit('dataLoaded');
			event.emit('loadUI');
		}
	},
};

const data = {
	actions,
	mutations,
	state,
};

export default data;
