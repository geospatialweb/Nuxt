import config from '../services/config';

const state = () => ({
	id: config.hillshade.id,
	active: config.hillshade.active,
	index: config.hillshade.index,
	layer: {
		type: config.hillshade.layer.type,
		url: config.hillshade.layer.url,
	},
	source: config.hillshade.source,
	type: config.hillshade.type,
});

const mutations = {
	setActive(state) {
		state.active = !state.active;
	},
};

const actions = {
	setActive({ commit }) {
		commit('setActive');
	},
};

const hillshade = {
	actions,
	mutations,
	state,
};

export default hillshade;
