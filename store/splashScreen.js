import config from '../services/config';

const state = () => ({
	id: config.splashScreen.id,
	active: config.splashScreen.active,
});

const mutations = {
	setActive(state) {
		state.active = !state.active;
	},
};

const actions = {
	setActive({ commit }) {
		commit('setActive');
	},
};

const getters = {
	splashScreen: state => state,
};

const splashScreen = {
	actions,
	getters,
	mutations,
	state,
};

export default splashScreen;
