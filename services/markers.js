import mapboxgl from 'mapbox-gl';
import event from './events';
import { dispatch, map, markers, popup } from './store';

export default {
	addMarkers(id) {
		if (!markers.markers[markers.markersHash[id]].active) {
			dispatch('markers/setActive', id);
		}

		markers.markers[markers.markersHash[id]]
			.map(marker => event.emit('addTo', marker));
	},
	displayMarkers() {
		markers.markers.map((marker) => {
			if (marker.active) {
				const id = marker[0].getElement().classList[0].replace('-marker', '');
				this.addMarkers(id);
			}
			return true;
		});
	},
	hideMarkers() {
		markers.markers.map((marker) => {
			const id = marker[0].getElement().classList[0].replace('-marker', '');
			const el = document.querySelector(`div.${id}-marker`);

			if (el) {
				this.removeMarkers(id, el);
			}
			return true;
		});
	},
	removeMarkers(id, el) {
		if (!el) {
			dispatch('markers/setActive', id);
		}

		markers.markers[markers.markersHash[id]]
			.map(marker => marker.remove());
	},
	/* create individual marker elements per each marker group and add mouse event handlers */
	setMarker(marker, data) {
		const array = [];
		array.active = marker.active;

		data.features.map((feature) => {
			const el = document.createElement('div');
			el.className = `${marker.id}-marker`;
			el.addEventListener('mouseenter', () => {
				if (popup.map) {
					event.emit('removePopup', map.id);
				}

				event.emit('addMarkerPopup', marker.id, feature);
			});
			el.addEventListener('mouseleave', () => {
				event.emit('removePopup', marker.id);
			});

			switch (marker.id) {
			case 'office':
			case 'places':
				array.push(
					new mapboxgl.Marker(el)
						.setLngLat(feature.geometry.coordinates),
				);
				break;
			case 'trails':
				array.push(
					new mapboxgl.Marker(el)
						.setLngLat([feature.properties.lng, feature.properties.lat]),
				);
				break;
			default:
			}
			return true;
		});
		dispatch('markers/setMarkers', array);
	},
	toggleMarkers(marker) {
		marker.active ?
			this.addMarkers(marker.id) :
			this.removeMarkers(marker.id);
	},
};
