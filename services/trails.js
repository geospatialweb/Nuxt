import event from './events';
import { dispatch, trails } from './store';

export default {
	selectTrail(evt) {
		const trail = evt.target.value;
		const i = trails.trails.findIndex(obj => obj.name === trail);
		/* exclude 'Select Trail' */
		if (i > 0) {
			dispatch('trails/setActive', i);
			event.emit('flyTo', trails.trails[i]);
		}
	},
};
