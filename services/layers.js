import event from './events';
import { $router, dispatch, layers } from './store';

export default {
	selectLayer(evt) {
		const layer = evt.target.classList[0];
		const i = layers.layers.findIndex(obj => obj.id === layer);
		this.setLayer(layer, i);
	},
	setLayer(layer, i) {
		dispatch('layers/setActive', i);

		switch (layer) {
		case 'satellite':
			/* hide active markers when changing map styles for aesthetic purposes */
			event.emit('hideMarkers');
			/* toggle between 'outdoors' and 'satellite' map styles (basemaps) */
			event.emit('setMapStyle');
			/* show active markers when changing map styles for aesthetic purposes */
			setTimeout(() => event.emit('displayMarkers'), 1200);
			break;
		case 'biosphere':
		case 'trails':
			dispatch('layerStyles/setActive', layer);
			event.emit('setLayerVisibility', layers.layers[i]);

			if (layer === 'trails') {
				event.emit('toggleMarkers', layers.layers[i]);
			}
			break;
		case 'office':
		case 'places':
			event.emit('toggleMarkers', layers.layers[i]);
			break;
		case 'heatmap':
			$router.push('/heatmap');
			break;
		default:
		}
	},
};
