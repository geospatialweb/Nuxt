import event from './events';
import { layerStyles } from './store';

export default {
	addLayerStyles() {
		layerStyles.layerStyles.map(layerStyle => event.emit('addLayer', layerStyle));
	},
};
