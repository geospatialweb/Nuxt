/* eslint-disable no-undef */
export const { $router } = $nuxt.$store;
export const { data } = $nuxt.$store.state;
export const { dispatch } = $nuxt.$store;
export const { heatmap } = $nuxt.$store.state;
export const { hillshade } = $nuxt.$store.state;
export const { layers } = $nuxt.$store.state;
export const { layerStyles } = $nuxt.$store.state;
export const { map } = $nuxt.$store.state;
export const { markers } = $nuxt.$store.state;
export const { popup } = $nuxt.$store.state;
export const { splashScreen } = $nuxt.$store.state;
export const { trails } = $nuxt.$store.state;
