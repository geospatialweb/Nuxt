import { Axios } from 'axios-observable';
import { fromEvent } from 'rxjs';
import event from './events';
import heatmapService from './heatmap';
import layersService from './layers';
import layerStylesService from './layerStyles';
import mapService from './map';
import markersService from './markers';
import popupService from './popup';
import { dispatch, heatmap, hillshade, layerStyles, map, splashScreen, trails } from './store';
import trailsService from './trails';

export default {
	setObservables() {
		const addLayer = fromEvent(event, 'addLayer')
			.subscribe((layerStyle) => {
				mapService.addLayer(layerStyle);
			},
			(err) => {
				console.error('addLayer Failed:\n', err.stack);
			},
			() => {
				addLayer.unsubscribe();
			});

		const addLayerStyles = fromEvent(event, 'addLayerStyles')
			.subscribe(() => {
				layerStylesService.addLayerStyles();
			},
			(err) => {
				console.error('addLayerStyles Failed:\n', err.stack);
			},
			() => {
				addLayerStyles.unsubscribe();
			});

		const addMapPopup = fromEvent(event, 'addMapPopup')
			.subscribe((evt) => {
				popupService.addMapPopup(evt);
			},
			(err) => {
				console.error('addMapPopup Failed:\n', err.stack);
			},
			() => {
				addMapPopup.unsubscribe();
			});

		const addMarkerPopup = fromEvent(event, 'addMarkerPopup')
			.subscribe((popup) => {
				popupService.addMarkerPopup(popup[0], popup[1]);
			},
			(err) => {
				console.error('addMarkerPopup Failed:\n', err.stack);
			},
			() => {
				addMarkerPopup.unsubscribe();
			});

		const addTo = fromEvent(event, 'addTo')
			.subscribe((obj) => {
				mapService.addTo(obj);
			},
			(err) => {
				console.error('addTo Failed:\n', err.stack);
			},
			() => {
				addTo.unsubscribe();
			});

		const displayMarkers = fromEvent(event, 'displayMarkers')
			.subscribe(() => {
				markersService.displayMarkers();
			},
			(err) => {
				console.error('displayMarkers Failed:\n', err.stack);
			},
			() => {
				displayMarkers.unsubscribe();
			});

		const flyTo = fromEvent(event, 'flyTo')
			.subscribe((trail) => {
				mapService.flyTo(trail);
			},
			(err) => {
				console.error('flyTo Failed:\n', err.stack);
			},
			() => {
				flyTo.unsubscribe();
			});

		const hideMarkers = fromEvent(event, 'hideMarkers')
			.subscribe(() => {
				markersService.hideMarkers();
			},
			(err) => {
				console.error('hideMarkers Failed:\n', err.stack);
			},
			() => {
				hideMarkers.unsubscribe();
			});

		const loadHeatmap = fromEvent(event, 'loadHeatmap')
			.subscribe(() => {
				heatmapService.loadHeatmap();
			},
			(err) => {
				console.error('loadHeatmap Failed:\n', err.stack);
			},
			() => {
				loadHeatmap.unsubscribe();
			});

		const loadLayerStyles = Axios.get('/api/layerStyles')
			.subscribe((res) => {
				Object.keys(res.data).map((key) => {
					const params = {
						fields: res.data[key].fields,
						table: res.data[key].layer.id,
					};
					const getLayerStyle = Axios.get('/api/geojson', { params })
						.subscribe((data) => {
							if (data.data) {
								const layerStyle = res.data[key].layer;
								layerStyle.source.data = data.data;
								dispatch('layerStyles/setLayerStyle', layerStyle);

								if (key === trails.id) {
									const marker = {
										id: res.data[key].layer.id,
										active: res.data[key].layer.active,
									};

									event.emit('setMarker', marker, data.data);
								}
							} else {
								console.error('getLayerStyle Data Error:\n', data.data);
							}
						},
						(err) => {
							console.error('getLayerStyle Failed:\n', err.stack);
						},
						() => {
							if (layerStyles.layerStyles.length === Object.keys(res.data).length) {
								dispatch('data/loaded', layerStyles.id);
							}

							getLayerStyle.unsubscribe();
						});
					return true;
				});
			},
			(err) => {
				console.error('loadLayerStyles Failed:\n', err.stack);
			},
			() => {
				loadLayerStyles.unsubscribe();
			});

		const loadMap = fromEvent(event, 'loadMap')
			.subscribe((id) => {
				id === map.id ?
					mapService.loadMap() :
					heatmapService.loadMap();
			},
			(err) => {
				console.error('loadMap Failed:\n', err.stack);
			},
			() => {
				loadMap.unsubscribe();
			});

		const loadMarkers = Axios.get('/api/markers')
			.subscribe((res) => {
				Object.keys(res.data).map((key) => {
					const params = {
						fields: res.data[key].fields,
						table: res.data[key].id,
					};
					const getMarker = Axios.get('/api/geojson', { params })
						.subscribe((data) => {
							data.data ?
								event.emit('setMarker', res.data[key], data.data) :
								console.error('getMarker Data Error:\n', data.data);
						},
						(err) => {
							console.error('getMarker Failed:\n', err.stack);
						},
						() => {
							getMarker.unsubscribe();
						});
					return true;
				});
			},
			(err) => {
				console.error('loadMarkers Failed:\n', err.stack);
			},
			() => {
				loadMarkers.unsubscribe();
			});

		const loadTrails = Axios.get('/api/trails')
			.subscribe((res) => {
				dispatch('trails/setTrails', res.data);
			},
			(err) => {
				console.error('loadTrails Failed:\n', err.stack);
			},
			() => {
				loadTrails.unsubscribe();
			});

		const loadUI = fromEvent(event, 'loadUI')
			.subscribe(() => {
				event.emit('setUIActive');
				event.emit('addLayerStyles');
				event.emit('displayMarkers');
			},
			(err) => {
				console.error('loadUI Failed:\n', err.stack);
			},
			() => {
				loadUI.unsubscribe();
			});

		const removePopup = fromEvent(event, 'removePopup')
			.subscribe((id) => {
				popupService.removePopup(id);
			},
			(err) => {
				console.error('removePopup Failed:\n', err.stack);
			},
			() => {
				removePopup.unsubscribe();
			});

		const resetHeatmapParams = fromEvent(event, 'resetHeatmapParams')
			.subscribe(() => {
				heatmapService.resetParams();
			},
			(err) => {
				console.error('resetHeatmapParams Failed:\n', err.stack);
			},
			() => {
				resetHeatmapParams.unsubscribe();
			});

		const resetHeatmapSettings = fromEvent(event, 'resetHeatmapSettings')
			.subscribe(() => {
				heatmapService.resetSettings();
			},
			(err) => {
				console.error('resetHeatmapSettings Failed:\n', err.stack);
			},
			() => {
				resetHeatmapSettings.unsubscribe();
			});

		const selectLayer = fromEvent(event, 'selectLayer')
			.subscribe((evt) => {
				layersService.selectLayer(evt);
			},
			(err) => {
				console.error('selectLayer Failed:\n', err.stack);
			},
			() => {
				selectLayer.unsubscribe();
			});

		const selectTrail = fromEvent(event, 'selectTrail')
			.subscribe((evt) => {
				trailsService.selectTrail(evt);
			},
			(err) => {
				console.error('selectTrail Failed:\n', err.stack);
			},
			() => {
				selectTrail.unsubscribe();
			});

		const setLayerVisibility = fromEvent(event, 'setLayerVisibility')
			.subscribe((layerStyle) => {
				mapService.setLayerVisibility(layerStyle);
			},
			(err) => {
				console.error('setLayerVisibility Failed:\n', err.stack);
			},
			() => {
				setLayerVisibility.unsubscribe();
			});

		const setMapStyle = fromEvent(event, 'setMapStyle')
			.subscribe(() => {
				mapService.setMapStyle();
			},
			(err) => {
				console.error('setMapStyle Failed:\n', err.stack);
			},
			() => {
				setMapStyle.unsubscribe();
			});

		const setMarker = fromEvent(event, 'setMarker')
			.subscribe((marker) => {
				markersService.setMarker(marker[0], marker[1]);
			},
			(err) => {
				console.error('setMarker Failed:\n', err.stack);
			},
			() => {
				setMarker.unsubscribe();
			});

		const setUIActive = fromEvent(event, 'setUIActive')
			.subscribe(() => {
				// dispatch('layers/setLayersActive');
				// dispatch('trails/setTrailsActive');

				if (heatmap.active) {
					dispatch('heatmap/setActive');
				}

				if (hillshade.active) {
					dispatch('hillshade/setActive');
				}

				if (splashScreen.active) {
					dispatch('splashScreen/setActive');
				}
			},
			(err) => {
				console.error('setUIActive Failed:\n', err.stack);
			},
			() => {
				setUIActive.unsubscribe();
			});

		const toggleMarkers = fromEvent(event, 'toggleMarkers')
			.subscribe((marker) => {
				markersService.toggleMarkers(marker);
			},
			(err) => {
				console.error('toggleMarkers Failed:\n', err.stack);
			},
			() => {
				toggleMarkers.unsubscribe();
			});
	},
};
