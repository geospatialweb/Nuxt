import mapboxgl from 'mapbox-gl';
import event from './events';
import { dispatch, map, trails } from './store';

export default {
	popup: {},
	addMapPopup(evt) {
		this.popup = new mapboxgl.Popup({
			closeButton: false,
			offset: 5,
		})
			.setHTML(`<div class='bold'>${evt.features[0].properties.name}</div><div>${evt.features[0].properties.description}</div>`)
			.setLngLat(evt.lngLat);

		event.emit('addTo', this.popup);
		dispatch('popup/setMapPopupActive');
	},
	addMarkerPopup(id, feature) {
		this.popup = new mapboxgl.Popup({
			closeButton: false,
			offset: 15,
		})
			.setHTML(`<div class='bold'>${feature.properties.name}</div><div>${feature.properties.description}</div>`);

		id === trails.id ?
			this.popup.setLngLat([feature.properties.lng, feature.properties.lat]) :
			this.popup.setLngLat(feature.geometry.coordinates);

		event.emit('addTo', this.popup);
		dispatch('popup/setMarkerPopupActive');
	},
	removePopup(id) {
		id === map.id ?
			dispatch('popup/setMapPopupActive') :
			dispatch('popup/setMarkerPopupActive');

		this.popup.remove();
	},
};
