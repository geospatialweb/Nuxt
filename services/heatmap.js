/*
   URL: https://gist.github.com/Pessimistress/1a4f3f5eb3b882ab4dd29f8ac122a7be
   Title: deck.gl + Mapbox HexagonLayer Example
   Author: Xiaoji Chen (@pessimistress)
   Data URL: https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/3d-heatmap/heatmap-data.csv
   Data Source: https://data.gov.uk
*/
import * as d3 from 'd3';
import mapboxgl from 'mapbox-gl';
import { HexagonLayer } from '@deck.gl/layers';
import { MapboxLayer } from '@deck.gl/mapbox';
import { dispatch, heatmap, map } from './store';

export default {
	settings: {},
	loadHeatmap() {
		this.heatmap = new MapboxLayer({
			id: heatmap.id,
			type: HexagonLayer,
			colorRange: heatmap.props.colourRange,
			coverage: heatmap.props.coverage,
			data: d3.csv(heatmap.props.url),
			elevationRange: heatmap.props.elevationRange,
			elevationScale: heatmap.props.elevationScale,
			extruded: heatmap.props.extruded,
			getPosition: d => [Number(d.lng), Number(d.lat)],
			lightSettings: heatmap.props.lightSettings,
			opacity: heatmap.props.opacity,
			radius: heatmap.props.radius,
			upperPercentile: heatmap.props.upperPercentile,
		});
		this.heatmap.active = heatmap.active;
		dispatch('data/loaded', heatmap.id);
	},
	loadMap() {
		/* instantiate map instance */
		this.map = new mapboxgl.Map(heatmap.settings)
			.addControl(new mapboxgl.NavigationControl(), map.controls.navigationControl.position)
			.on('load', () => {
				this.loadParams();
				this.addLayer();
			})
			.on('render', () => {
				this.setSettings();
			});
	},
	loadParams() {
		heatmap.params.map((param) => {
			document.getElementById(param).oninput = (evt) => {
				dispatch('heatmap/setParams', [param, evt.target.value]);
				this.heatmap.setProps({ [param]: heatmap[param] });
			};
			return true;
		});
	},
	addLayer() {
		this.map.addLayer(this.heatmap, heatmap.index);
		this.setLayerVisibility();
	},
	setLayerVisibility() {
		dispatch('heatmap/setActive');
		this.heatmap.active = heatmap.active;
		this.map.setLayoutProperty(heatmap.id, 'visibility', 'visible');
	},
	setSettings() {
		this.settings.bearing = this.map.getBearing();
		this.settings.center = this.map.getCenter();
		this.settings.pitch = this.map.getPitch();
		this.settings.zoom = this.map.getZoom();
		dispatch('heatmap/setSettings', this.settings);
	},
	resetParams() {
		heatmap.params.map((param) => {
			this.heatmap.setProps({ [param]: heatmap[param] });
			return true;
		});
	},
	resetSettings() {
		this.map.setBearing(heatmap.settings.bearing);
		this.map.setCenter(heatmap.settings.center);
		this.map.setPitch(heatmap.settings.pitch);
		this.map.setZoom(heatmap.settings.zoom);
	},
};
