import mapboxgl from 'mapbox-gl';
import event from './events';
import { data, dispatch, hillshade, layerStyles, map, popup } from './store';

export default {
	settings: {},
	loadMap() {
		if (!mapboxgl.accessToken) {
			mapboxgl.accessToken = map.accessToken;
		}
		/* instantiate map instance */
		this.map = new mapboxgl.Map(map.settings)
			.addControl(new mapboxgl.NavigationControl(), map.controls.navigationControl.position)
			.on('click', () => {
				if (popup.map) {
					event.emit('removePopup', map.id);
				}
			})
			.on('load', () => {
				!data.heatmap ?
					event.emit('loadHeatmap') :
					event.emit('loadUI');

				this.addHillShade();
			})
			.on('render', () => {
				this.setSettings();
			});
	},
	/* add DEM hillshade to 'outdoors' map style */
	addHillShade() {
		if (map.style.id === map.styles.outdoors.id) {
			dispatch('hillshade/setActive');
			this.map.addSource(hillshade.source, hillshade.layer);
			this.addLayer(hillshade);
		}
	},
	addLayer(layer) {
		this.map.addLayer(layer, layer.index);

		if (layer.id === layerStyles.biosphere.id) {
			this.map
				.on('click', layer.id, (evt) => {
					if (!popup.map) {
						event.emit('addMapPopup', evt);
					}
				})
				.on('mouseenter', layer.id, () => {
					this.map.getCanvas().style.cursor = 'pointer';
				})
				.on('mouseleave', layer.id, () => {
					this.map.getCanvas().style.cursor = '';
				});
		}

		this.setLayerVisibility(layer);
	},
	setLayerVisibility(layer) {
		layer.active ?
			this.map.setLayoutProperty(layer.id, 'visibility', 'visible') :
			this.map.setLayoutProperty(layer.id, 'visibility', 'none');
	},
	setMapStyle() {
		if (popup.map) {
			event.emit('removePopup', map.id);
		}

		dispatch('map/setActiveStyle', map.style.id);
		this.map.setStyle(map.style.url);
		/* add hillshade & layerStyles after 1 sec delay to set map style */
		setTimeout(() => {
			map.style.id === map.styles.satellite.id ?
				dispatch('hillshade/setActive') :
				this.addHillShade();

			event.emit('addLayerStyles');
		}, 1000);
	},
	setSettings() {
		this.settings.bearing = this.map.getBearing();
		this.settings.bounds = this.map.getBounds();
		this.settings.center = this.map.getCenter();
		this.settings.pitch = this.map.getPitch();
		this.settings.zoom = this.map.getZoom();
		this.settings.style = map.style.url;
		dispatch('map/setSettings', this.settings);
	},
	addTo(obj) {
		obj.addTo(this.map);
	},
	flyTo(trail) {
		this.map.flyTo({
			center: trail.center,
			zoom: trail.zoom,
		});
	},
};
