import express from 'express';
import fs from 'fs';
import morgan from 'morgan';
import { resolve } from 'path';
// PostgreSQL 11 PostGIS 2.5
import routes from './routes';
// PostgreSQL 12 PostGIS 3.0
// import routes from './routes/index_postgis_3';

require('dotenv').config();

express()
	.use(express.static(resolve(process.env.PUBLIC_ROOT)))
	.use('/api', routes)
	.use(morgan('combined', {
		stream: fs.createWriteStream(resolve('./logs/server.log'), {
			flags: 'a',
		}),
	}))

	.set('env', process.env.NODE_ENV)
	.set('host', process.env.HOST || '0.0.0.0')
	.set('port', process.env.PORT || 80)

	.listen(process.env.PORT, process.env.HOST, (err) => {
		err ?
			console.error('Server Failed:\n', err) :
			console.log(`Active on http://localhost:${process.env.PORT} at ` +
                `${new Date().toDateString()} ${new Date().toTimeString()}`);
	});
