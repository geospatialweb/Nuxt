## Geospatial Web

http://www.geospatialweb.ca

Development sandbox website to showcase the code integration of Node, Express, Vue, Vuex, Nuxt, RxJs, Mapbox GL, Deck.GL, PostGIS and Docker. This implementation features dedicated map instances in separate routes. The deck.gl experimental class ```MapboxLayer``` implemented here is currently supported in Google Chrome only.