const pkg = require('./package');

module.exports = {
	mode: 'spa',
	head: {
		title: 'Geospatial Web',
		meta: [
			{ charset: 'utf-8' },
			{ hid: 'author', name: 'author', content: pkg.author },
			{ hid: 'description', name: 'description', content: pkg.description },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' },
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: './favicon.ico' },
			{ rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Orbitron:800' },
			{ rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300' },
		],
	},
	/* Doc: https://nuxtjs.org/guide/assets */
	css: [
		'~/assets/global.scss',
		'~/assets/mapbox-gl.css',
	],
	/* Doc: https://nuxtjs.org/guide/plugins */
	plugins: [
		'~/plugins/rxjs',
	],
	/* Doc: https://github.com/nuxt-community/proxy-module */
	modules: [
		'@nuxtjs/proxy',
	],
	proxy: {
		'/api': { target: 'http://localhost' },
	},
	/* Doc: https://nuxtjs.org/api/configuration-build */
	build: {
		extend(config, { isDev, isClient }) {
			isClient ?
				config.devtool = 'eval-source-map' :
				config.devtool = 'inline-source-map';
			/* run ESLint on save */
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/,
				});
			}
		},
	},
};
