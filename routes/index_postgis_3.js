import express from 'express';
import { Pool } from 'pg';
import layerStyles from '../data/layerStyles_postgis_3';
import markers from '../data/markers_postgis_3';
import trails from '../data/trails';

const createFeatureCollection = (features) => {
	const fc = {
		type: 'FeatureCollection',
		features: [],
	};

	features.map((feature) => {
		fc.features.push(JSON.parse(feature.geojson));
		return true;
	});

	return fc;
};

export default express.Router()
	.get('/geojson', (req, res) => {
		const query = `SELECT ST_AsGeoJSON(feature.*) AS geojson
      FROM (
        SELECT ${req.query.fields}
        FROM ${req.query.table}
      ) AS feature`;

		const pool = new Pool({
			/* local instance process.env.DATABASE_URL_LOCAL */
			connectionString: process.env.DATABASE_URL_LOCAL,
		})
			.on('error', (err) => {
				console.error('Connection Failed:\n', err);
				process.exit(-1);
			});

		pool.query(query, (err, rows) => {
			if (err) {
				console.error('Query Failed:\n', err);
			} else if (rows.rowCount > 0) {
				const fc = createFeatureCollection(rows.rows);
				res.status(200).json(fc);
			} else {
				console.error('No rows found:\n', query);
			}
			pool.end();
		});
	})
	.get('/layerStyles', (req, res) => res.status(200).json(layerStyles))
	.get('/markers', (req, res) => res.status(200).json(markers))
	.get('/trails', (req, res) => res.status(200).json(trails));
