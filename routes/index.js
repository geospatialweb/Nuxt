import express from 'express';
import { Pool } from 'pg';
import layerStyles from '../data/layerStyles';
import markers from '../data/markers';
import trails from '../data/trails';

const deleteFeaturesProperty = (features) => {
	features.map((feature) => {
		if (feature.properties.st_asgeojson) {
			Reflect.deleteProperty(feature.properties, 'st_asgeojson');
		}
		return feature;
	});
};

export default express.Router()
	.get('/geojson', (req, res) => {
		const query = 'SELECT row_to_json(fc) ' +
			'FROM (SELECT \'FeatureCollection\' As type, array_to_json(array_agg(f)) As features ' +
			'FROM (SELECT\'Feature\' As type, ' +
				'ST_AsGeoJSON(t.geom)::json As geometry, ' +
				`row_to_json((SELECT p FROM (SELECT ${req.query.fields}) As p)) As properties ` +
			`FROM ${req.query.table} As t) As f) As fc`;

		const pool = new Pool({
			/* local instance process.env.DATABASE_URL_LOCAL */
			connectionString: process.env.DATABASE_URL,
		})
			.on('error', (err) => {
				console.error('Connection Failed:\n', err);
				process.exit(-1);
			});

		pool.query(query, (err, rows) => {
			if (err) {
				console.error('Query Failed:\n', err);
			} else if (rows.rowCount > 0) {
				const fc = rows.rows[0].row_to_json;
				deleteFeaturesProperty(fc.features);
				res.status(200).json(fc);
			} else {
				console.error('No rows found:\n', query);
			}
			pool.end();
		});
	})
	.get('/layerStyles', (req, res) => res.status(200).json(layerStyles))
	.get('/markers', (req, res) => res.status(200).json(markers))
	.get('/trails', (req, res) => res.status(200).json(trails));
